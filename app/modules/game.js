import Card from './card'
export default class Game {
    cards = [];
    emoji = ["🧙‍","️🙀","🎃","😈","👻","🍎","🦇","💀","👽","🧟‍"];
    indexOfOpenedCard = null;
    cardsCount = 0;
    score = 0;
    matchCounter = 0;
    
    constructor() {
        this.createCardsArray();
    }

    flipCard = (index) => {
        if(this.cards[index].isFliped === false 
          && this.cards[index].isMatched === false) {
            this.checkIfCardsMatches();
            this.indexOfOpenedCard = index;
            this.cards[index].image = this.cards[index].emoji;
            this.cards[index].isFliped = true;
            this.cardsCount = this.cardsCount + 1;
            // i'll fix it in future
            if(this.matchCounter >= 7){
              this.matchCounter = this.matchCounter + 0.5;
              if(this.matchCounter === 8) this.score = this.score + 2;
            } 
          }
    }

    checkIfCardsMatches = () =>{
      if(this.cardsCount >= 2){
        for(i = 0; i< this.cards.length; i++){
          if(this.cards[i].isFliped) {
            if(this.indexOfOpenedCard !== null && this.indexOfOpenedCard !== i){
              if(this.cards[this.indexOfOpenedCard].emoji === this.cards[i].emoji){
                this.cards[this.indexOfOpenedCard].isMatched = true;
                this.cards[this.indexOfOpenedCard].emoji = "";
                this.cards[this.indexOfOpenedCard].image = "";
                this.cards[i].isMatched = true;
                this.cards[i].emoji = "";
                this.cards[i].image = "";
                this.score = this.score + 2;
                this.matchCounter = this.matchCounter + 1;
              } else if (this.cards[this.indexOfOpenedCard].isSeen 
                | this.cards[i].isSeen){
                  // because "true" transforms to 1 and "false" to 0:
                  this.score = this.score - this.cards[this.indexOfOpenedCard].isSeen - this.cards[i].isSeen;
              } else {
                this.cards[this.indexOfOpenedCard].isSeen = true;
                this.cards[i].isSeen = true;
              }
            } 
          }
          this.cards[i].isFliped = false;
          this.cards[i].image = "";
        }
        this.indexOfOpenedCard = null;
        this.cardsCount = 0;
      }
    }

    createCardsArray = () => {
        cards = [];
        emojiChoises = this.generateCards();
        for(i=0;i<emojiChoises.length;i++){
            card = new Card(emojiChoises[i]);
            cards.push(card);
        }
        this.cards = cards;
    }

    generateCards = () => {
        emojiSet = [...this.emoji];
        emojis= [];
        for(i=0;i<8;i++){
          rand = this.getRandomInt(0, emojiSet.length-1);
          emojis.push(emojiSet[rand]);
          emojis.push(emojiSet[rand]);
          emojiSet.splice(rand, 1);
        }
        this.shuffle(emojis);
        return emojis;
      }
    
      getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }
    
      shuffle = (a) => {
        for (let i = a.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
      }
}
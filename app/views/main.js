import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, SafeAreaView} from 'react-native';
import CardView from '../components/cardView';
import Game from '../modules/game';

export default class MainView extends Component{

  game = new Game();

  constructor(props) {
    super(props);
    this.state = {
        cardsMap: null,
        score: 0,
        isFliped: false,
    }
  }
  
  onClick = (index) => {
    this.game.flipCard(index);
    this.getCards(this.game);

  }

  setTheme = (card) => {
    if(card.isMatched){
      return styles.cardViewMatched;
    } else if (card.isFliped) {
      return styles.cardView;
    } else {
      return styles.cardViewPressed;
    }
  }

  componentDidMount() {
    this.getCards(this.game);
  }

  getCards = (game) => {
    let cardsMap = null;
    if (game.cards) {
        cardsMap = game.cards.map((item, index) => {
            return <CardView 
                key={index} index={index} 
                onClick={this.onClick} 
                text={game.cards[index].image} 
                style={this.setTheme(game.cards[index])}/> 
        });
    }
    this.setState({ cardsMap });
  }

  startNewGame = () =>{
    this.game = new Game();
    this.getCards(this.game);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={styles.mainView}>
            {this.state.cardsMap}
          </View>
        </View>
        <Text style={styles.scoreLabel}>
          {this.game.matchCounter < 8 ? "Score: "+ this.game.score : "You win! Your score: "+ this.game.score}
        </Text>
        <TouchableOpacity onPress={this.startNewGame}>
          <Text style={styles.scoreLabel}>{"Start again"}</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    width: '100%', 
    height: '100%', 
    flexDirection: 'row', 
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#daf2fd',
   },
  scoreLabel: {
    fontSize: 30,
  },
  cardView: {
    width: '20%',
    height: '20%',
    marginHorizontal: 5,
    marginVertical: 5,
    borderColor: 'black',
    borderWidth: 2,
    backgroundColor: 'white',
    borderRadius:5
  },
  cardViewPressed: {
    width: '20%',
    height: '20%',
    marginHorizontal: 5,
    marginVertical: 5,
    borderColor: 'black',
    borderWidth: 2,
    backgroundColor: 'blue',
    borderRadius:5
  },
  cardViewMatched: {
    width: '20%',
    height: '20%',
    marginHorizontal: 5,
    marginVertical: 5,
    borderColor: "transparent",
    borderWidth: 2,
    backgroundColor: 'transparent',
    borderRadius:5
  },
});

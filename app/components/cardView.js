import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

export default class CardView extends Component {
    constructor(props) {
        super(props);
    }

    onClick =()=>{
        const { index, onClick} = this.props;
        onClick(index);
    }

    render() {
        const { text, style} = this.props;
        return (
            <View style={style}>
                <TouchableOpacity onPress = {this.onClick} style={styles.content}>
                    <Text style={styles.textStyle}>
                        {text}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 50
    },
    content: {
        flex:1, 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius: 5
    }
  });